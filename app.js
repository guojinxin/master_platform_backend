import "./src/config/env";
import {routers} from "./src/routers";
import './src/config/awsConfig'
import './src/cornJob/schedule/index';
const bodyParser = require('body-parser');
const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const app = express();
/* 跨域设置 */
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({limit: "2100000kb"}));
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));


app.use(routers);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});
module.exports = app;
