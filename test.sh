#!/usr/bin/env bash
echo "----------------- STARTING MOCHA TEST -----------------"

echo "----------------- STOPPING OLD MONGO DB CONTAINER -----------------"

docker stop mongo-4-pms

echo "----------------- REMOVING OLD MONGO DB CONTAINER -----------------"

docker rm mongo-4-pms

echo "----------------- RUNNING NEW MONGO DB CONTAINER -----------------"

docker run -p 27017:27017 --name mongo-4-pms -d mongo:4.0.4 --auth

echo "----------------- WAITTING NEW MONGO DB CONTAINER FULL UP -----------------"

secs=$((5))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

echo "----------------- CREATING USER AND GRANTING ADMIN ROLE -----------------"

docker exec mongo-4-pms mongo admin --eval "db.createUser({user: 'fintelics', pwd: 'b0ad5!Es%51wd', roles: [{role: 'root', db: 'admin'}]});"

echo "----------------- INSTALLING NODE MODULE -----------------"

npm install

echo "----------------- STOPPING OLD BACKEND CONTAINER -----------------"

docker stop savants-backend

echo "----------------- REMOVING OLD BACKEND CONTAINER -----------------"

docker rm savants-backend

echo "----------------- STOPPING OLD BACKEND IMAGE -----------------"

docker rmi savants-backend

echo "----------------- BUILDING NEW BACKEND IMAGE WITH TEST DOCKERFILE -----------------"

docker build -t savants-backend -f Dockerfile.test .

echo "----------------- RUNNING NEW BACKEND IMAGE -----------------"

docker run -d --name savants-backend -p 8084:8084 --link=mongo-4-pms:127.0.0.1  savants-backend

echo "----------------- WAITTING NEW BACKEND IMAGE FULL UP -----------------"

secs=$((15))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

echo "----------------- CHECKING CONTAINER AND DB CONNECTION WITH LOG -----------------"

docker logs savants-backend

echo "----------------- STARTING MOCHA TEST -----------------"

yarn

echo "----------------- END MOCHA TEST -----------------"

echo "----------------- STOPPING OLD BACKEND CONTAINER -----------------"

docker stop savants-backend

echo "----------------- REMOVING OLD BACKEND CONTAINER -----------------"

docker rm savants-backend

echo "----------------- STOPPING OLD BACKEND IMAGE -----------------"

docker rmi savants-backend

echo "----------------- STOPPING MONGO DB CONTAINER -----------------"

docker stop mongo-4-pms

echo "----------------- REMOVING MONGO DB CONTAINER -----------------"

docker rm mongo-4-pms

echo "----------------- END MOCHA TEST -----------------"
