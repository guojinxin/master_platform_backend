export const responseToJson = (response, status, description, data = {}) => {
    return response.status(status).json({
        status,
        description,
        data,
    });
}
