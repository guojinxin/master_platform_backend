import {STS, config} from 'aws-sdk'

config.update(setting);
config.region = "us-east-1";

const sts = new STS({
  apiVersion: 'latest',
});

export const getSessionToken = () => {
  const params = {
    DurationSeconds: 3600,
    SerialNumber: "YourMFASerialNumber",
    TokenCode: "123456"
  };
  sts.getSessionToken(params, (err, data) => {
    if (err) {
      console.log(err)
    }
    {
      console.log(data)
    }
  })
}

