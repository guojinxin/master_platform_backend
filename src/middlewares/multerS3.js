import multer from "multer";
import multerS3 from "multer-s3";
import aws from "aws-sdk";
import uuid from 'node-uuid';
const { s3, s3bucket } = require("../config/awsConfig");
// const s3 = new aws.S3();
export const multerVideoUpload = multer({
  storage: multerS3({
    s3: s3,
    bucket: s3bucket,
    acl: "public-read-write",
    metadata: function(req, file, cb) {
      if (file.filename == "") {
        return;
      }
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      req.filename = "course/" + req.userId.replace("@", "(at)") +  "/video/" + uuid.v1() + ".mp4";
      cb(null, req.filename);
    }
  })
}).array("file", 1);

export const multerPicturesUpload = multer({
  storage: multerS3({
    s3: s3,
    bucket: s3bucket,
    acl: "public-read-write",
    metadata: function(req, file, cb) {
      if (file.filename == "") {
        return;
      }
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      req.filename = "course/" + req.userId.replace("@", "(at)") + "/preview/" + uuid.v1() + ".png";
      cb(null, req.filename);
    }
  })
});
export const avatarUpload = multer({
  storage: multerS3({
    s3: s3,
    bucket: s3bucket,
    acl: "public-read-write",
    metadata: function(req, file, cb) {
      if (file.filename == "") {
        return;
      }
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      req.filename = "user/" + req.userId.replace("@", "(at)") + "/preview/" + uuid.v1() + ".png";
      cb(null, req.filename);
    }
  })
});
export const pdfUpload = multer({
  storage: multerS3({
    s3: s3,
    bucket: s3bucket,
    acl: "public-read-write",
    metadata: function(req, file, cb) {
      if (file.filename == "") {
        return;
      }
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      req.filename =  "course/"+ req.userId.replace("@", "(at)") +"/file/" + uuid.v1() +".pdf";
      cb(null, req.filename);
    }
  })
});

export const multerBannerUpload = multer({
  storage: multerS3({
    s3: s3,
    bucket: s3bucket,
    acl: "public-read-write",
    metadata: function(req, file, cb) {
      if (file.filename == "") {
        return;
      }
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      req.filename = "banner/preview/" + uuid.v1() + ".png";
      cb(null, req.filename);
    }
  })
});
