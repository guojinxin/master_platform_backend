import jwt from "jsonwebtoken";
import jwksJson from "../config/jwks.json";
import _ from "lodash";
const jwkToPem = require("jwk-to-pem");
const logger = require("pino")();
export const isHasAccess = (req, res, next) => {
  try {
    /*************
     * Validate token
     **************/
    let token = req.headers["authorization"];
    if (!token) {
      return res.status(401).send({
        status: 401,
        description: "Missing authorization token"
      });
    }
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        if (err.name === "TokenExpiredError") {
          return res.status(401).send({
            status: 401,
            description: "Expired authorization token"
          });
        }
        return res.status(401).send({
          status: 401,
          description: "Invalid authorization token"
        });
      }
      req.userId = decoded.userId;
      req.role = decoded.role;
      console.info(`Request: ${req.method} ${req.path} User=${req.userId}`);
      return next();
    });

  } catch (error) {
    console.error(error);
    return res.status(401).send(error);
  }
}
export const authentication = (req, res, next) => {
  try {
    let userEmail = "";
    if (!req.headers.authorization) {
      return res.status(401).send({
        status: 401,
        description: "Missing authorization header",
      });
    }
    const token = req.headers.authorization;
    const keys = jwksJson["keys"];
    const pems = {};
    for (let i = 0; i < keys.length; i++) {
      //Convert each key to PEM
      const key_id = keys[i].kid;
      const modulus = keys[i].n;
      const exponent = keys[i].e;
      const key_type = keys[i].kty;
      const jwk = { kty: key_type, n: modulus, e: exponent };
      const pem = jwkToPem(jwk);
      pems[key_id] = pem;
    }
    //validate the token
    const decodedJwt = jwt.decode(token, { complete: true });
    if (!decodedJwt) {
      return res.status(401).send("Not a valid JWT token");
    }
    const kid = decodedJwt.header.kid;
    const pem = pems[kid];
    if (!pem) {
      return res.status(401).send("Invalid token");
    }

    jwt.verify(token, pem, function (err, payload) {

      if (err) {
        return res.status(401).send("Invalid token");
      } else {
        if (!_.isEmpty(payload.email)) {
          userEmail = payload.email;
          req.userId = userEmail.toLowerCase();
        } else {
          userEmail = payload.username;
          req.userId = userEmail.toLowerCase();
        }
        logger.info(`Request: ${req.method} ${req.path} User=${req.userId}`);
        next();
      }
    });
  } catch (error) {
    return res.status(401).send(error);
  }
};
