import schedule from 'node-schedule';
import {nextCourseDate} from "../courses";

let rule = new schedule.RecurrenceRule();
rule.minute = [0,30];

function scheduleCourse() {
  schedule.scheduleJob(rule, async function () {
    await nextCourseDate()
  })
}
scheduleCourse();
