import {ProductModel} from "../../models/ProductModel";
import {ProductReviewModel} from "../../models/ProductReviewModel";

export const nextCourseDate = async () => {
  let Review = await ProductReviewModel.aggregate([
    {
      $group: { _id: "$productId", star: { $avg: "$star" } }
    }
  ]);
  Review.map(async item => {
    await ProductModel.updateOne({ _id: item._id }, { averageStar: item.star });
    await ProductModel.updateOne(
      { courseId: item._id },
      { averageStar: item.star }
    );
  });
  console.log("over to change next course");
};
