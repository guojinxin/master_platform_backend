const aws = require('aws-sdk')
const {AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY,AWS_REGION,S3_BUCKET,AWS_USER_POOL_ID,AWS_CLIENT_ID,AWS_USER_POOL_REGION,AWS_COGNITO_AUTH_URL} = process.env
if (AWS_ACCESS_KEY_ID && AWS_SECRET_ACCESS_KEY) {
  aws.config.update({
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
    accessKeyId: AWS_ACCESS_KEY_ID,
    region: AWS_REGION
  });
}
const s3 = new aws.S3();

module.exports = {
  cognitoConfig: function () {
    let cognitoConfig = {
      UserPoolId: AWS_USER_POOL_ID,
      ClientId: AWS_CLIENT_ID,
      PoolRegion: AWS_USER_POOL_REGION,
      JwksUrl: AWS_COGNITO_AUTH_URL
    }
    return cognitoConfig
  },
  aws,
  s3,
  s3bucket: S3_BUCKET,
}
