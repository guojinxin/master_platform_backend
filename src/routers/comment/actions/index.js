import { responseToJson } from "../../../utils/formatResponse";
import commentdb from "../../../utilities/commentdb";
import _,{isEmpty} from "lodash";
import { ProductModel } from "../models/ProductModel";

//添加评论˙
export const addComments = async (req, res) => {
  try {
    const createdById = req.userId;
    const {productId, star, reviewMessage} = req.body;
    const product = await ProductModel.findOne({productId});
    if (isEmpty(product)) {
      return responseToJson(res, 201, "We didn't find this product");
    }
    const result = await commentdb.addComments({productId, star, reviewMessage, createdById})
    return responseToJson(res, 200, "success", result);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};