import express from "express";
import {
  addComments
} from "./actions";


export const comment = express.Router();

comment.post("/addComments", addComments);

