import _, { isEmpty } from "lodash";
import { responseToJson } from "../../../utils/formatResponse";
import supermarketdb from "../../../utilities/supermarketdb";

//查询用户信息
export const getSupermarket = async (req, res) => {
  try {
    let { supermarketId } = req.params;
    let createById = req.userId;
    if (!createById) {
      return responseToJson(res, 400, "error", "Not enough parameter");
    }
    const data = await supermarketdb.getSupermarket({
      supermarketId,
      createById,
    });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const addSupermarke = async (req, res) => {
  try {
    const createById = req.userId;
    const { title, imageUrl } = req.body;
    const data = await supermarketdb.addSupermarke({
      title,
      imageUrl,
      createById,
    });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error);
  }
};

export const updateSupermarket = async (req, res) => {
  try {
    const createById = req.userId;
    const { supermarketId } = req.params;
    const { title, imageUrl } = req.body;
    const data = await supermarketdb.updateSupermarket({
      createById,
      supermarketId,
      title,
      imageUrl,
    });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const supermarketUpload = async (req, res) => {
  let courseFile = `https://${process.env.S3_BUCKET}.s3.${process.env.AWS_USER_POOL_REGION}.amazonaws.com/${req.filename}`;
  return responseToJson(res, 200, "success", courseFile);
};