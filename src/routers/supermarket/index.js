import express from "express";
import {
  getSupermarket,
  addSupermarke,
  updateSupermarket
} from "./actions";
import {multerPicturesUpload} from "../../middlewares/multerS3";

export const supermarket = express.Router();


supermarket.get('/:supermarketId/getSupermarket',getSupermarket);
supermarket.post("/addSupermarke", addSupermarke);
supermarket.put('/:supermarketId/updateSupermarket',updateSupermarket);
supermarket.post("/supermarketUpload", multerPicturesUpload.single("file"), supermarketUpload);
