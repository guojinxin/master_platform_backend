import express from "express";

import {login,createPartner} from "./actions/login"

export const sign = express.Router();

sign.post('/login',login)
sign.post('/partner',createPartner)
