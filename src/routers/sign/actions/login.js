import { responseToJson } from "../../../utils/formatResponse";
import {AdminModel} from "../../../models/AdminModel";
import _ from "lodash"
import jwt from "jsonwebtoken";

export const login = async (req, res) => {
  try {
    const {username, password} = req.body;
    if (!username) {
      return responseToJson(res, 400, "Missing Username");
    }
    if (!password) {
      return responseToJson(res, 400, "Missing Password");
    }
    const user = await AdminModel.findOne({
      username
    });
    if (!user) {
      return responseToJson(res, 400, "Invalid username or password");
    }
    const adminModel = new AdminModel(user);
    /* const generateHash = await adminModel.generateHash(password)
    console.log("哈希值转换", generateHash)*/
    // 哈希值比较
    const validResult = await adminModel.validPassword(password);
    if (validResult) {
      const {JWT_SECRET, JWT_EXPIRE_HOURS} = process.env;
      let token = jwt.sign({userId: user._id, role: user.role}, JWT_SECRET, {
        expiresIn: JWT_EXPIRE_HOURS // expires in 24 hours
      });
      return responseToJson(res, 200, "Login Success", {
        data: {message: "login success", role: user.role, token}
      });
    } else {
      return responseToJson(res, 400, "Invalid username or password");
    }
  } catch (e) {
    return responseToJson(res, 500, e.message);
  }
};


export const createPartner = async (req, res) => {
  try {
    const {username, password, role} = req.body;
    if (!username) {
      return responseToJson(res, 400, "Missing Username");
    }
    if (!password) {
      return responseToJson(res, 400, "Missing Password");
    }
    if (!role) {
      return responseToJson(res, 400, "Missing Role");
    }
    const user = await AdminModel.findOne({
      username
    });
    if (user) {
      return responseToJson(res, 400, "user name has been exist!");
    }
    const adminModel = new AdminModel(user);
    const generateHash = await adminModel.generateHash(password)
    const saveNewAdmin = new AdminModel({
      username,
      password: generateHash,
      role: role,
    })
    const result = await saveNewAdmin.save();
    return responseToJson(res, 200, `create ${role} success`);
  } catch (e) {
    console.log(e);
    return responseToJson(res, 500, "Internal error");
  }
}
