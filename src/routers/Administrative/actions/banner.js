import { responseToJson } from "../../../utils/formatResponse";
import bannerdb from "../../../utilities/bannerdb";
import _ from "lodash"
//查看banner
export const getBanner = async (req, res) => {
  try {
    await bannerdb
      .getBanner()
      .then((data) => {
        return responseToJson(res, 200, "success", data);
      })
      .catch((e) => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//查看添加createBanner
export const createBanner = async (req, res) => {
  const { imageUrl, title, description, link } = req.body;
  try {
    await bannerdb
      .createBanner(imageUrl, title, description, link)
      .then((data) => {
        return responseToJson(res, 200, "success", data);
      })
      
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//修改banner
export const updateBanner = async (req, res) => {
  const { bannerId } = req.params;
  const { imageUrl, title, description, link, status } = req.body;
  const params = {}; 
  if (_.find(imageUrl)) {
    params.imageUrl = imageUrl;
  } 
  if (_.find(title)) {
    params.title = title;
  } 
  if (_.find(description)) {
    params.description = description;
  } 
  if (_.find(link)) {
    params.link = link;
  } 
  if (_.find(status)) {
    params.status = status;
  }
  try {
    await bannerdb
      .updateBanner(bannerId, params)
      .then((data) => {
        return responseToJson(res, 200, "success", data);
      })
      .catch((e) => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//查看banner详情
export const getDetailsBanner = async (req, res) => {
  const { bannerId } = req.params;
  try {
    await bannerdb
      .getDetailsBanner(bannerId)
      .then((data) => {
        return responseToJson(res, 200, "success", data);
      })
      .catch((e) => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//上传图片
export const bannerUpload = async (req, res) => {
  let avatarUrl = `https://${process.env.S3_BUCKET}.s3.${process.env.AWS_USER_POOL_REGION}.amazonaws.com/${req.filename}`;
  return responseToJson(res, 200, "success", avatarUrl);
};