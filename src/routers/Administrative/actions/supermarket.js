import { responseToJson } from "../../../utils/formatResponse";
import ownerdb from "../../../utilities/ownerdb";
import _ from "lodash";

export const getSupermarket = async (req, res) => {
  try {
    const data = await ownerdb.getSupermarket();

    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
export const updateSupermarketStatus = async (req, res) => {
  try {
    const { supermarketId } = req.params;
    const { status, soldOutReason } = req.body;
    const data = await supermarketdb.updateSupermarketStatus({
      supermarketId,
      status,
      soldOutReason,
    });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
