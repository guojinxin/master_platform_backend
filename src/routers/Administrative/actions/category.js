import {responseToJson} from "../../../utils/formatResponse";
import admindb from "../../../utilities/admindb";
import {CategoryModel} from "../../../models/CategoryModel";

//类型查询
export const getCategoryTag = async (req, res) => {
  try {
    await CategoryModel.find({})
      .then(data => {
        return responseToJson(res, 200, "success", data);
      })
      .catch(e => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
//添加类型
export const addCategory = async (req, res) => {
  try {
    const {tags, category} = req.body.values;
    await admindb
      .addCategory(tags, category)
      .then(data => {
        return responseToJson(res, 200, "success", data);
      })
      .catch(e => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//删除类型
export const removeCategory = async (req, res) => {
  try {
    const {categoryId} = req.params;
    const {status} = req.body
    admindb
      .removeCategory(categoryId, status)
      .then(result => {
        return responseToJson(res, 200, "success", result);
      })
      .catch(err => {
        return responseToJson(res, 500, err.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
//查看类型详情
export const getDetailsCategory = async (req, res) => {
  try {
    const {categoryId} = req.params;
    admindb
      .getDetailsCategory(categoryId)
      .then(result => {
        return responseToJson(res, 200, "success", result);
      })
      .catch(err => {
        return responseToJson(res, 500, err.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//修改类型
export const updateCategory = async (req, res) => {
  try {
    const {categoryId} = req.params;
    const {tags, category} = req.body.values;
    admindb
      .updateCategory(categoryId, tags, category)
      .then(result => {
        return responseToJson(res, 200, "success", result);
      })
      .catch(err => {
        return responseToJson(res, 500, err.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
