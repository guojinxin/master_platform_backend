import express from "express";
import {
  getCategoryTag,
  addCategory,
  removeCategory,
  updateCategory,
  getDetailsCategory
} from "./actions/category";
import {
  createBanner,
  getBanner,
  updateBanner,
  getDetailsBanner,
  bannerUpload,
} from "./actions/banner";
import {
  getSupermarket,
  updateSupermarketStatus
} from "./actions/supermarket";
import { multerBannerUpload } from "../../middlewares/multerS3";

export const admin = express.Router();

admin.get("/getCategoryTag", getCategoryTag);
admin.post("/addCategory",addCategory)
admin.put("/:categoryId/removeCategory",removeCategory)
admin.put("/:categoryId/updateCategory",updateCategory)
admin.get("/:categoryId/getDetailsCategory",getDetailsCategory)
admin.post("/createBanner",createBanner)
admin.get("/getBanner",getBanner)
admin.put("/:bannerId/updateBanner",updateBanner)
admin.get("/:bannerId/getDetailsBanner",getDetailsBanner);
admin.post("/bannerUpload", multerBannerUpload.single("file"), bannerUpload);
admin.get("/getSupermarket", getSupermarket);
admin.put('/:supermarketId/updateSupermarket',updateSupermarketStatus);
