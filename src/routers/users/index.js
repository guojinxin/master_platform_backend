import express from "express";
import {
  getUserInfoByUserId,
  resetUserName,
  getUser,
  updateUser,
  updateAvatarUpload,
} from "./actions";
import { avatarUpload } from "../../middlewares/multerS3";

export const user = express.Router();

user.get("/getUserInfoByUserId", getUserInfoByUserId);
user.post("/resetUserName", resetUserName);
user.put("/updateUserId", updateUser);
user.get("/userId", getUser);
user.post("/avatarUpload", avatarUpload.single("file"), updateAvatarUpload);

