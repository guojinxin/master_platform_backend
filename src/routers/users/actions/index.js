import {UserModel} from "../../../models/UserModel";
import _, {isEmpty} from "lodash";
import {responseToJson} from "../../../utils/formatResponse";
import {createUserName} from "../../../utils/createUserName";
import userdb from "../../../utilities/userdb";

export const getUserInfoByUserId = async (req, res) => {
  try {
    let userId = req.userId;
    let user = await UserModel.findOne({
      userId,
    });
    if (isEmpty(user)) {
      const newUser = new UserModel({
        userName: "user_" + createUserName(6),
        userId,
      });
      user = await newUser.save();
    }
    return responseToJson(res, 200, "success", user);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const resetUserName = async (req, res) => {
  try {
    const userId = req.userId;
    const {userName} = req.body;
    if (userName.length < 3) {
      return responseToJson(
        res,
        203,
        "Your name should be no less than 3 characters"
      );
    }
    let user = await UserModel.findOne({
      userId,
    });
    if (isEmpty(user)) {
      // 找不到用户
      return responseToJson(res, 403, "Can not found this user");
    }
    let updateUser = await UserModel.findOneAndUpdate(
      {userId},
      {userName},
      {new: true}
    );
    return responseToJson(res, 200, "success", updateUser);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//查询用户信息
export const getUser = async (req, res) => {
  try {
    let userId = req.userId;
    if (!userId) {
      return responseToJson(res, 400, "error", "Not enough parameter");
    }
    const data = await userdb.getUser(userId)
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

//修改用户信息
export const updateUser = async (req, res) => {
  try {
    let userId = req.userId;
    if (!userId)
      return responseToJson(res, 400, "error", "Not enough parameter");
    const data = await userdb.updateUser(userId, req.body)
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
//上传图片
export const updateAvatarUpload = async (req, res) => {
  try {
    let userId = req.userId;
    let avatarUrl = `https://${process.env.S3_BUCKET}.s3.${process.env.AWS_USER_POOL_REGION}.amazonaws.com/${req.filename}`;
    if (!userId)
      return responseToJson(res, 400, "error", "Not enough parameter");
    db.updateAvatarUser(userId, avatarUrl)
      .then((data) => {
        return responseToJson(res, 200, "success", avatarUrl);
      })
      .catch((e) => {
        return responseToJson(res, 500, e.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};