import "../models/db";
import express from "express";
import { admin } from "./Administrative";
import { owner } from "./owner";
import { user } from "./users";
import { userInfo } from "./userinfo";
import {supermarket} from "./supermarket";
import {product} from "./product"

import {
  authentication,
  isHasAccess,
} from "../middlewares/authentication";
import { sign } from "./sign";

export const routers = express.Router();
routers.use("/admin", isHasAccess, admin);
routers.use("/sign", sign);
routers.use("/owner", owner);
routers.use("/user", user);

routers.use(authentication);
routers.use("/userInfo", userInfo);
routers.use("/supermarket", supermarket);
routers.use("/product", product);
