import { responseToJson } from "../../../utils/formatResponse";
import productdb from "../../../utilities/productdb";
import _ from "lodash";

export const getProduct = async (req, res) => {
  const createById = req.userId;
  try {
    const data = await productdb.getProduct({ createById });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const addProduct = async (req, res) => {
  try {
    const createById = req.userId;
    const {
      title,
      imageUrl,
      inventory,
      freight,
      purchaseNumber,
      describe,
      price,
      weight,
      categoryId,
      supermarketId
    } = req.body;
    const data = await productdb.addProduct({
      title,
      imageUrl,
      inventory,
      freight,
      purchaseNumber,
      describe,
      price,
      weight,
      categoryId,
      createById,
      supermarketId
    });
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error);
  }
};

export const updateProduct = async (req, res) => {
  try {
    const { productId } = req.params;
    const {
      title,
      imageUrl,
      inventory,
      freight,
      purchaseNumber,
      describe,
      price,
      weight,
      categoryId,
      supermarketId
    } = req.body;
    productdb
      .updateProduct({
        productId,
        title,
        imageUrl,
        inventory,
        freight,
        purchaseNumber,
        describe,
        price,
        weight,
        categoryId,
        supermarketId
      })
      .then((result) => {
        return responseToJson(res, 200, "success", result);
      })
      .catch((err) => {
        return responseToJson(res, 500, err.message);
      });
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
