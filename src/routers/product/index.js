import express from "express";
import {
  getProduct,
  addProduct,
  updateProduct
} from "./actions";


export const product = express.Router();

product.get("/getProduct", getProduct);
product.post("/addProduct", addProduct);
product.put("/:productId/updateProduct", updateProduct);
