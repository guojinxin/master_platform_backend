import express from "express";
import {
  updateUserInfo
} from "./actions";

export const userInfo = express.Router();


userInfo.put('/:userId/updateUserInfo',updateUserInfo);
