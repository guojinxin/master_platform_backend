import {UserModel} from "../../../models/UserModel";
import _, {isEmpty} from "lodash";
import {responseToJson} from "../../../utils/formatResponse";
import userdb from "../../../utilities/userdb";

export const updateUserInfo = async (req, res) => {
  try {
    const {userId} = req.params;
    const {firstName,lastName,gender,phone,email,dateOfBirth,address,province,country,role} = req.body;
    let user = await UserModel.findOne({
      userId,
    });
    if (isEmpty(user)) {
      return responseToJson(res, 400, "error", "The user does not exist.");
    }
    const data = await userdb.updateUserInfo({userId,firstName,lastName,gender,phone,email,dateOfBirth,address,province,country,role});
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};
