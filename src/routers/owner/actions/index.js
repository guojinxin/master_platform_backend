import { responseToJson } from "../../../utils/formatResponse";
import ownerdb from "../../../utilities/ownerdb";
import _ from "lodash";

export const getHome = async (req, res) => {
  try {
    const banner = await ownerdb.getBanner();
    const product = await ownerdb.getHome();
    const data = { banner, product };
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const getSearchProduct = async (req, res) => {
  try {
    let {content} = req.params;
    if (!isEmpty(content)) {
      const data = await ownerdb.getSearchProduct({content});
      return responseToJson(res, 200, "success", data);
    }
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};

export const getSupermarket = async (req, res) => {
  try {
    const data = await ownerdb.getSupermarket();
   
    return responseToJson(res, 200, "success", data);
  } catch (error) {
    return responseToJson(res, 500, error.message);
  }
};