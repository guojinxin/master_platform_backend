import express from "express";
import {
  getHome,
  getSearchProduct,
  getSupermarket
} from "./actions";


export const owner = express.Router();

owner.get("/getHome", getHome);
owner.get("/:content/getSearchProduct", getSearchProduct);
owner.get("/getSupermarket", getSupermarket);
