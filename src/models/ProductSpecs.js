 /**
 * @name: Ryan
 * @description: ProductSpecsSchema
 * @created at : 2020-02-28 16:04:48
 */
import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ProductSpecsSchema = new Schema(
  {
    userId: {
      type: String,
    },
    productId: {
      type: String,
    },
    sku_price:{
      type:Number,
      default: 0,
    },
    sku_weight:{
      type:Number,
      default: 0,
    },
    sku_stock:{
      type:Number,
      default: 0,
    },
  },
  {
    timestamps: true
  }
);

export const productSpecsModel = mongoose.model("productSpecs", ProductSpecsSchema, "productSpecs");
