import mongoose from "mongoose";

const Schema = mongoose.Schema;
const productReviewSchema = new Schema(
  {
    productId: { type: String },
    star: {
      type: Number,
      required: true,
    },
    reviewMessage: {
      type: String,
      required: true,
      default: 0,
    },
    createdById: {
      type: String,
    },
  },
  { timestamps: true }
);

export const ProductReviewModel = mongoose.model("productReview", productReviewSchema, "productReview");
