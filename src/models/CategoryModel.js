import mongoose from "mongoose";

const Schema = mongoose.Schema;
const categorySchema = new Schema(
  {
    tags: { type: String },
    status: {
      type: String,
      default: "soldOut",
      enum: ["putaway", "soldOut"]
    },
  },
  { timestamps: true }
);

export const CategoryModel = mongoose.model(
  "category",
  categorySchema,
  "category"
);
