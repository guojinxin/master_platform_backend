import mongoose from "mongoose";

const MongoDB_Url = process.env.MONGODB_URL;

connectMongo();

function connectMongo() {
  mongoose.connect(MongoDB_Url, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true}, (err) => {
    if (!err) {
      console.info(MongoDB_Url + "Connected mongoDB server success ")
    } else {
      connectMongo()
      console.error(err)
    }
  })
}

module.exports = mongoose;
