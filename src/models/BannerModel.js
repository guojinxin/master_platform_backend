import mongoose from "mongoose";

const Schema = mongoose.Schema;
const bannerSchema = new Schema(
  {
    imageUrl: { type: String },
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    link: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      default: "soldOut",
      enum: ["putaway", "soldOut"],
    },
  },
  { timestamps: true }
);

export const BannerModel = mongoose.model(
  "banner",
  bannerSchema,
  "banner"
);
