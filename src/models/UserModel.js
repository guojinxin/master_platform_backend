/**
 * @name: Ryan
 * @description: 用户
 * @created at : 2020-02-28 16:58:30
 */
import mongoose from "mongoose";
import moment from "moment";

const Schema = mongoose.Schema;
const {DEFAULT_USER_AVATAR} = process.env
const userSchema = new Schema(
  {
    userId: {
      type: String,
    },
    firstName: {
      type: String,
      required: true,
      default: "",
    },
    lastName: {
      type: String,
      default: "",
      required: true
    },
    avatar: {
      type: String,
      default: DEFAULT_USER_AVATAR,
      require: true,
    },
    gender: {
      type: String,
      enum: ["female", "male"],
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    email:{
      type:String,
      required: true
    },
    dateOfBirth: {
      type:Date,
      required: true
    },
    address:{
      type:String,
      required: true
    },
    province:{
      type:String,
      required: true
    },
    country: {
      type:String,
      required: true
    },
    role:{
      type:String,
      enum: ["merchant", "user"],
      required: true
    }
  },
  {
    timestamps: true,
  }
);

export const UserModel = mongoose.model("user", userSchema, 'user');
