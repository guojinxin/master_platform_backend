import mongoose from "mongoose";

const Schema = mongoose.Schema;
const orderSchema = new Schema(
  {
    status: {
      type: String,
      enum: ["paid", "init", "cancel", "refund"]
    },
    finalPrice: {
      type: Number,
      required: true
    },
    productId: {
      type: String,
    },
    userId: {
      type: String,
    },
    amount:{
      type:Number,
      default:0
    }
  },
  { timestamps: true }
);

export const OrderModel = mongoose.model(
  "order",
  orderSchema,
  "order"
);
