 /**
 * @name: Ryan
 * @description: productSchema
 * @created at : 2020-02-28 16:04:48
 */
import mongoose from "mongoose";

const Schema = mongoose.Schema;
const productSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String,
      required: true
    },
    supermarketId:String,
    inventory:{
      type:Number,
      default: 0,
    },
    status: {
      type: String,
      default: "check",
      enum: ["putaway", "soldOut", "check"]
    },
    freight:{
      type:Number,
      default: 0,
    },
    purchaseNumber:{
      type:Number,
      default: 0,
    },
    describe: {
      type:String,
    },
    price:{
      type:Number,
      default: 0,
    },
    weight:{
      type:Number,
      default: 0,
    },
    categoryId: [{
      id:String,
      categoryId:String,
      categoryName:String
    }],
    createById: String,
    // 平均评分，向上取整，每半小时计算一次，方便查询
    averageStar: {
      type: Number,
      default: 0
    },
  },
  {
    timestamps: true
  }
);

export const ProductModel = mongoose.model("product", productSchema, "product");
