 /**
 * @name: Ryan
 * @description: Supermarket
 * @created at : 2020-02-28 16:04:48
 */
import mongoose from "mongoose";

const Schema = mongoose.Schema;
const supermarketSchema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String,
      required: true
    },
    status: {
      type: String,
      default: "check",
      enum: ["putaway", "soldOut", "check"]
    },
    // 下架原因
    soldOutReason: {
      type: String,
      default: ""
    },
    createById: String,
  },
  {
    timestamps: true
  }
);

export const SupermarketModel = mongoose.model("supermarket", supermarketSchema, "supermarket");
