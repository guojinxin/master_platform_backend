import mongoose from "mongoose";
import bcrypt from 'bcryptjs';

const Schema = mongoose.Schema;
const MONGO_SALT = process.env.MONGO_SALT;
const adminSchema = new Schema({
    username: {
        type: String,
    },
    password: {
        type: String,
    },
    role:{
        type:String,
        enum:["admin","partner"]
    }
})

adminSchema.methods.generateHash = function generateHash(value) {
    return bcrypt.hash(value, MONGO_SALT);
};

adminSchema.methods.validPassword = function validPassword(password) {
    return bcrypt.compare(password, this.password);
};

// 当model传入参数为两个的时候
// model 里面的第一个参数 要注意：1.首字母大写 2.要和数据库集合名称对应
// 这个模型会和模型名称相对应的复数名称相等
export const AdminModel = mongoose.model('admin', adminSchema, 'admin');
