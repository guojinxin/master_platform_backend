import _ from "lodash";
import { ProductModel } from "../models/ProductModel";

async function getProduct({ createById }) {
  return await ProductModel.aggregate([
    { $match: { createById } },
    { $addFields: { productId: { $toString: "$_id" } } },
    {
      $lookup: {
        from: "productSpecs",
        localField: "productId",
        foreignField: "productId",
        as: "productSpecs",
      },
    },
  ]);
}
async function addProduct({
  title,
  imageUrl,
  inventory,
  freight,
  purchaseNumber,
  describe,
  price,
  weight,
  categoryId,
  createById,
  supermarketId
}) {
  const productModel = new ProductModel({
    title,
    imageUrl,
    inventory,
    freight,
    purchaseNumber,
    describe,
    price,
    weight,
    categoryId,
    createById,
    supermarketId
  });
  return await productModel.save();
}

async function updateProduct({
  productId,
  title,
  imageUrl,
  inventory,
  freight,
  purchaseNumber,
  describe,
  price,
  weight,
  categoryId,
}) {
  return await ProductModel.findOneAndUpdate(
    { _id: productId },
    {
      title,
      imageUrl,
      inventory,
      freight,
      purchaseNumber,
      describe,
      price,
      weight,
      categoryId,
    },
    {
      new: true,
    }
  );
}
module.exports.getProduct = getProduct;
module.exports.addProduct = addProduct;
module.exports.updateProduct = updateProduct;
