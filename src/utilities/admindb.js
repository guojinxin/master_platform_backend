import { CategoryModel } from "../models/CategoryModel";
import _ from "lodash";

async function addCategory(tag, category) {
  const tags = tag.split(",");
  const categoryModel = new CategoryModel({
    tags,
    category
  });
  return await categoryModel.save();
}
async function removeCategory(categoryId, status) {
  return await CategoryModel.findOneAndUpdate(
    { _id: categoryId },
    { status: status },
    {
      new: true
    }
  );
}
async function getDetailsCategory(categoryId) {
  return await CategoryModel.findOne({ _id: categoryId });
}
async function updateCategory(categoryId, tag, category) {
  const tags = tag.split(",");
  return CategoryModel.findOneAndUpdate(
    { _id: categoryId },
    { tags, category },
    {
      new: true
    }
  );
}

module.exports.addCategory = addCategory;
module.exports.removeCategory = removeCategory;
module.exports.updateCategory = updateCategory;
module.exports.getDetailsCategory = getDetailsCategory;
