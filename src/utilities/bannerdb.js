import { BannerModel } from "../models/BannerModel";
import _ from "lodash";

async function getBanner() {
  return await BannerModel.find({});
}

async function getDetailsBanner(bannerId) {
  return await BannerModel.findOne({_id:bannerId});
}
async function createBanner(imageUrl, title, description, link) {
  const bannerModel = new BannerModel({
    imageUrl,
    title,
    description,
    link
  });
  return await bannerModel.save();
}
async function updateBanner(bannerId, params) {
  return await BannerModel.findOneAndUpdate(
    { _id: bannerId },
    params,
    {
      new: true
    }
  );
}
module.exports.getBanner = getBanner;
module.exports.createBanner = createBanner;
module.exports.updateBanner = updateBanner;
module.exports.getDetailsBanner = getDetailsBanner;
