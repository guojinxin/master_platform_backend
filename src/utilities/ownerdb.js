import _ from "lodash";
import { BannerModel } from "../models/BannerModel";
import { ProductModel } from "../models/ProductModel";
import { SupermarketModel } from "../models/SupermarketModel";

async function getBanner() {
  return await BannerModel.find({});
}
async function getSupermarket() {
  return await SupermarketModel.find({});
}
async function getProduct() {
  return await ProductModel.aggregate([
    { $match: {} },
    { $addFields: { productId: { $toString: "$_id" } } },
    { $addFields: { supermarket_id: { $toObjectId: "$supermarketId" } } },
    {
      $lookup: {
        from: "productReview", 
        localField: "productId", 
        foreignField: "productId",
        as: "productReview", 
      },
    },
    {
      $lookup: {
        from: "supermarket", 
        localField: "supermarket_id", 
        foreignField: "_id", 
        as: "supermarket", 
      },
    },
    {
      $lookup: {
        from: "productSpecs", 
        localField: "productId", 
        foreignField: "productId", 
        as: "productSpecs", 
      },
    },
  ]);
}
async function getSearchProduct({content}) {
  return await ProductModel.aggregate([
    {
      $match: {
        $or: [
          {title: {$regex: content}},
          {categoryId: {$regex: content}},
          {averageStar: {$regex: content}}
        ]
      }
    },
    { $addFields: { productId: { $toString: "$_id" } } },
    { $addFields: { supermarket_id: { $toObjectId: "$supermarketId" } } },
    {
      $lookup: {
        from: "productReview", 
        localField: "productId", 
        foreignField: "productId",
        as: "productReview", 
      },
    },
    {
      $lookup: {
        from: "supermarket", 
        localField: "supermarket_id", 
        foreignField: "_id", 
        as: "supermarket", 
      },
    },
    {
      $lookup: {
        from: "productSpecs", 
        localField: "productId", 
        foreignField: "productId", 
        as: "productSpecs", 
      },
    },
  ]);
}

module.exports.getBanner = getBanner;
module.exports.getProduct = getProduct;
module.exports.getSearchProduct = getSearchProduct;
module.exports.getSupermarket = getSupermarket;
