import { UserModel } from "../models/UserModel";

async function getUser(userId) {
  return UserModel.find({ userId: userId });
}
async function updateUser(userId, data) {
  return await UserModel.findOneAndUpdate({ userId: userId }, data, {
    new: true,
  });
}

async function updateUserInfo({
  userId,
  firstName,
  lastName,
  gender,
  phone,
  email,
  dateOfBirth,
  address,
  province,
  country,
  role,
}) {
  return await UserModel.findOneAndUpdate(
    { userId: userId },
    {
      firstName,
      lastName,
      gender,
      phone,
      email,
      dateOfBirth,
      address,
      province,
      country,
    },
    {
      new: true,
    }
  );
}
module.exports.getUser = getUser;
module.exports.updateUser = updateUser;
module.exports.updateUserInfo = updateUserInfo;
