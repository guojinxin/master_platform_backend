import { ProductReviewModel } from "../models/ProductReviewModel";
import _ from "lodash";

async function addComments({productId, star, reviewMessage, createdById}) {
  const productReviewModel = new ProductReviewModel({
    productId,
    star,
    reviewMessage,
    createdById
  });
  return await productReviewModel.save();
}

module.exports.addComments = addComments;
