import _ from "lodash";
import { SupermarketModel } from "../models/SupermarketModel";

async function getSupermarket({ supermarketId, createById }) {
  return await SupermarketModel.findOne({ _id: supermarketId, createById });
}

async function addSupermarke({ title, imageUrl }) {
  const supermarketModel = new SupermarketModel({
    title,
    imageUrl,
    createById,
  });
  return await supermarketModel.save();
}
async function updateSupermarket({
  createById,
  supermarketId,
  title,
  imageUrl,
}) {
  return await SupermarketModel.findOneAndUpdate(
    { _id: supermarketId, createById },
    {
      title,
      imageUrl,
    },
    {
      new: true,
    }
  );
}

async function updateSupermarketStatus({
  supermarketId,
  status,
  soldOutReason,
}) {
  return await SupermarketModel.findOneAndUpdate(
    { _id: supermarketId },
    {
      status,
      soldOutReason,
    },
    {
      new: true,
    }
  );
}
module.exports.getSupermarket = getSupermarket;
module.exports.addSupermarke = addSupermarke;
module.exports.updateSupermarket = updateSupermarket;
module.exports.updateSupermarketStatus = updateSupermarketStatus;
